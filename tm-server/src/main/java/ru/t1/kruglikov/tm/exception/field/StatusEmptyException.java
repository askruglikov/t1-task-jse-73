package ru.t1.kruglikov.tm.exception.field;

public final class StatusEmptyException extends AbstractFieldExceprion {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}
