package ru.t1.kruglikov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.enumerated.UserSort;
import ru.t1.kruglikov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    List<User> findAll(@Nullable UserSort sort);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeOneByLogin(@Nullable String login);

    @Nullable
    User removeOneByEmail(@Nullable String email);

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    User lockOneByLogin(@Nullable String login);

    @NotNull
    User unlockOneByLogin(@Nullable String login);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

}
