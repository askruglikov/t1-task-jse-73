package ru.t1.kruglikov.tm.exception.field;

public final class NameEmptyException extends AbstractFieldExceprion {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}
