package ru.t1.kruglikov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldExceprion {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
