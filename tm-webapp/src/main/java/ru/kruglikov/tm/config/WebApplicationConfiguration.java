package ru.kruglikov.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.kruglikov.tm.api.endpoint.IProjectRestEndpoint;
import ru.kruglikov.tm.api.endpoint.ITaskRestEndpoint;
import ru.kruglikov.tm.api.endpoint.IAuthEndpoint;
import javax.xml.ws.Endpoint;


@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private Bus bus;

    @Bean
    public Endpoint projectEndpointRegistry(
            @NotNull final IProjectRestEndpoint projectEndpoint,
            @NotNull final Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(
            @NotNull final ITaskRestEndpoint taskEndpoint,
            @NotNull final Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(
            @NotNull final IAuthEndpoint authEndpoint,
            @NotNull final Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/ws/*").permitAll()
                .antMatchers("/api/auth/login").permitAll()

                .and()
                .exceptionHandling()
                .authenticationEntryPoint(new ServiceAuthenticationEntryPoint())
                .and()
                .authorizeRequests()

                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/auth")
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }

}
