package ru.t1.kruglikov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.kruglikov.tm.listener.AbstractListener;
import ru.t1.kruglikov.tm.enumerated.Role;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
